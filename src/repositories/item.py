from src.models import Item
from src.configs import redis_client
from typing import List
from uuid import uuid4

class ItemRepository:
    def find_one(self, id: str):
        item = redis_client.hgetall("item:" + id)
        return item


    def find_many(self):
        items_id = redis_client.lrange("item_list", 0, -1)
        items: List[Item] = []

        for id in items_id:
            items.append(redis_client.hgetall("item:" + id))

        return items


    def insert(self, item):
        id = str(uuid4())
        title = item.title
        description = item.description

        redis_client.hset("item:" + id, "id", id)
        redis_client.hset("item:" + id, "title", title)
        redis_client.hset("item:" + id, "description", description)
        redis_client.rpush("item_list", id)

        return { "inserted": True, "id": id }


    def delete(self, id: str):
        status = redis_client.delete("item:" + id)
        redis_client.lrem("item_list", 1, id)
        return status

itemRepository = ItemRepository()
