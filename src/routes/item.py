from pydantic import BaseModel
from typing import List
from fastapi import APIRouter
from src.models import Item
from uuid import UUID
from fastapi.exceptions import HTTPException
from src.configs import redis_client
from src.repositories import itemRepository
from fastapi.responses import JSONResponse
import json

class ItemRequest(BaseModel):
    title: str
    description: str


class ItemResponse(BaseModel):
    id: str
    title: str
    description: str


router = APIRouter(prefix="/items")

@router.get("/", response_model=List[ItemResponse])
def getItemList():
    items = itemRepository.find_many()
    return items


@router.get("/{item_id}", response_model=ItemResponse)
def getItem(item_id: str):
    item = itemRepository.find_one(item_id)
    
    if item == None or len(item) == 0:
        raise HTTPException(detail="404 Not Found", status_code=404)

    return item


@router.post("/", response_class=JSONResponse, status_code=201)
def createItem(item: ItemRequest):
    status = itemRepository.insert(item)
    return { "created": 1, "id": status["id"] }


@router.delete("/{item_id}", response_class=JSONResponse, status_code=200)
def deleteItem(item_id: str):
    status = itemRepository.delete(item_id)

    if status:
        return { "message": "ok", "status": 1 }

    return { "message": "nothing to delete", "status": 0 }






