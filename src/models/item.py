from pydantic import BaseModel
from typing import Optional
from uuid import UUID

class Item(BaseModel):
    id: Optional[str]
    title: str
    description: str
    user_id: Optional[str]