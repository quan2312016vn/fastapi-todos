from .config import get_configs
import redis
from os import environ

config = get_configs()

redis_client = redis.Redis(
    host=config.REDIS_HOST, 
    port=config.REDIS_PORT, 
    db=config.REDIS_DB,
    decode_responses=True
)

pong = redis_client.ping()
print("Ping status:",pong)