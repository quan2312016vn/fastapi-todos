from pydantic import BaseSettings
from functools import lru_cache

class Settings(BaseSettings):
    REDIS_HOST: str = ...
    REDIS_PORT: int = ...
    REDIS_DB: int = ...
    
    class Config:
        env_file = ".env"
        env_file_encoding = 'utf-8'
    
@lru_cache()
def get_configs():
    return Settings()