from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from src.routes import item_router

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins = ["*"],
    allow_methods = ["*"],
    allow_headers = ["*"]
)

app.include_router(item_router)

@app.exception_handler(RequestValidationError)
def handle_validation_error(_, exc):
    error = {
        "code": 422,
        "message": exc.errors(),
    }
    return JSONResponse(content=error, status_code=422)

