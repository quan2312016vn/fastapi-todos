# FastAPI Todos

## 1. Cài đặt Poetry 
Nếu chưa có poetry, cài đặt Poetry tại đây: https://python-poetry.org/docs/#installation

## 2. Install các dependency
Vào thư mục project, chạy:
```
poetry install
```

## 3. Chạy server demo:
Trước hết thêm các environment variable cần thiết vào .env (xem .env.example), sau đó khởi chạy server:
```
poetry run uvicorn src.app:app --reload --port=3000
```

### Note: Để xem API docs, truy cập http://localhost:3000/docs
